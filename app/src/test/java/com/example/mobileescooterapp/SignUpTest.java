package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class SignUpTest {

    private SignUp activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(SignUp.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void accountCreatedMethodTrueChangesActivity(){
        activity.accountCreated(true);

        Intent expectedIntent = new Intent(activity, AccountCreated.class);
        Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

    @Test
    public void accountCreatedMethodFalseShowsToast(){
        activity.accountCreated(false);
        ShadowLooper.idleMainLooper();
        assertEquals("Account not created", ShadowToast.getTextOfLatestToast().toString() );
    }

    @Test
    public void FieldsEmptyShowsToast(){
        Button signUpButton = (Button) activity.findViewById(R.id.sign_up_btn);
        EditText firstName = (EditText) activity.findViewById((R.id.editTextFirstName));
        EditText lastName = (EditText) activity.findViewById((R.id.editTextLastName));
        EditText email = (EditText) activity.findViewById((R.id.editTextEmailSignUp));
        EditText password = (EditText) activity.findViewById((R.id.editTextPasswordSignUp));

        activity.runOnUiThread(new Runnable() {
            public void run() {
                firstName.setText("");
                lastName.setText("");
                email.setText("");
                password.setText("");
                signUpButton.performClick();

                //Check that toast was shown
                ShadowLooper.idleMainLooper(Toast.LENGTH_LONG);
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Fields cannot be empty"));

            }
        });
    }

    @Test
    public void existingDetailsEnteredReturnsToast(){
        Button signUpButton = (Button) activity.findViewById(R.id.sign_up_btn);
        EditText firstName = (EditText) activity.findViewById((R.id.editTextFirstName));
        EditText lastName = (EditText) activity.findViewById((R.id.editTextLastName));
        EditText email = (EditText) activity.findViewById((R.id.editTextEmailSignUp));
        EditText password = (EditText) activity.findViewById((R.id.editTextPasswordSignUp));

        activity.runOnUiThread(new Runnable() {
            public void run() {
                firstName.setText("test");
                lastName.setText("test");
                email.setText("3test@test.com");
                password.setText("test");
                signUpButton.performClick();

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Check that toast was shown
                ShadowLooper.idleMainLooper(Toast.LENGTH_LONG);
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Account not created"));

            }
        });
    }


}