package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class SendFeedbackTest {

    private SendFeedback activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(SendFeedback.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void feedbackSentSuccessfully() {
        Button sendFeedBackButton = (Button) activity.findViewById(R.id.continue_sendfeedback_btn);//Initialise the button
        final EditText feedback = (EditText) activity.findViewById(R.id.enterFeedbackText);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                //This data exists in the database
                feedback.setText("test feedback");
                activity.CustomerID = "92";
                sendFeedBackButton.performClick();

                //Sleep required to give program time to query API
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent expectedIntent = new Intent(activity, MainMenu.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }
}