package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class EnterCodeTest {

    private EnterCode activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(EnterCode.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void validIDChangeActivity(){
        EditText scooterID = (EditText) activity.findViewById(R.id.ScooterIDText);
        Button confirm_btn = (Button) activity.findViewById(R.id.go_to_booking_btn);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                //This scooter is free
                activity.scooterID.setText("207");//todo:find valid scoot
                confirm_btn.performClick();

                //Sleep required to give program time to query API
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent expectedIntent = new Intent(activity, MakeBooking.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });

    }

    @Test
    public void takenScooterReturnsToast(){
        EditText scooterID = (EditText) activity.findViewById(R.id.ScooterIDText);
        Button confirm_btn = (Button) activity.findViewById(R.id.go_to_booking_btn);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                //This scooter is taken
                activity.scooterID.setText("101");
                confirm_btn.performClick();

                //Sleep required to give program time to query API
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                Tests the toast has been displayed
                ShadowLooper.idleMainLooper(1);
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Scooter Unavailable"));
            }
        });

    }

    @Test
    public void invalidIDReturnsToast(){
        EditText scooterID = (EditText) activity.findViewById(R.id.ScooterIDText);
        Button confirm_btn = (Button) activity.findViewById(R.id.go_to_booking_btn);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                //This scooter does not exist
                activity.scooterID.setText("3");
                confirm_btn.performClick();

                //Sleep required to give program time to query API
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                Tests the toast has been displayed
                ShadowLooper.idleMainLooper(1);
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Invalid scooterID."));
            }
        });

    }


}