package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.widget.Button;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class ScooterMapTest {

    private ScooterMap activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(ScooterMap.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }


    @Test
    public void BookingButtonClickedWithNoCurrentBookingChangesActivity() throws Exception
    {
        Button book_btn = (Button) activity.findViewById(R.id.makeMapBooking);
        //Customer ID 92 has no booking
        activity.CustomerID = "92";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                activity.locationID = 1;
                activity.currentSession = false;
                book_btn.performClick();
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                Intent expectedIntent = new Intent(activity, MakeBooking.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());


            }
        });
    }


    @Test
    public void BookingButtonWithCurrentBookingShowsToast() throws Exception
    {
        Button book_btn = (Button) activity.findViewById(R.id.makeMapBooking);
        //Customer ID 104 has no booking
        activity.CustomerID = "104";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                activity.currentSession = true;
                book_btn.performClick();
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ShadowLooper.idleMainLooper();
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("You already have a scooter booked. Please return this before making a new booking"));

            }
        });
    }



}