package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class AddCardTest extends AddCard{

    private AddCard activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(AddCard.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void cardAddedMethodTrueChangesActivity(){
        activity.cardAdded(true);
        Intent expectedIntent = new Intent(activity, MainMenu.class);
        Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

    @Test
    public void cardAddedMethodFalseShowsToast(){
        activity.cardAdded(false);
        ShadowLooper.idleMainLooper();
        assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Card was not added successfully"));
    }

    @Test
    public void invalidCardNumberShowsToast(){
        Button continueButton = (Button) activity.findViewById(R.id.continue_card_mainMenu_btn);
        continueButton.performClick();
        ShadowLooper.idleMainLooper();
        assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Card number is not long enough"));
    }

    @Test
    public void validDetailsChangeActivity(){
        EditText cardNumEditText = (EditText) activity.findViewById(R.id.cardNumberAddCard);
        EditText expiryDateEditText = (EditText) activity.findViewById(R.id.expiryDateAddCard);
        Button continueButton = (Button) activity.findViewById(R.id.continue_card_mainMenu_btn);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                //This data exists in the database
                activity.CustomerId = "92";
                cardNumEditText.setText("4444444444444444");
                expiryDateEditText.setText("2022-01-20");
                continueButton.performClick();

                //Sleep required to give program time to query API
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent expectedIntent = new Intent(activity, MainMenu.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });

    }
}