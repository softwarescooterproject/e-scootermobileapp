package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class MainMenuTest extends MainMenu{

    private MainMenu activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(MainMenu.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void bookScooterNoCurrentBookingChangesActivity() throws Exception
    {
        Button bookScooterBtn = (Button) activity.findViewById(R.id.book_scooter_btn);
        //Customer ID 92 does not have a booking
        activity.CustomerID = "92";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                bookScooterBtn.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent expectedIntent = new Intent(activity, EnterCode.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }

    @Test
    public void bookScooterHasCurrentBookingShowsToast() throws Exception
    {
        Button bookScooterBtn = (Button) activity.findViewById(R.id.book_scooter_btn);
        //Customer ID 92 does not have a booking
        activity.CustomerID = "104";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                bookScooterBtn.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ShadowLooper.idleMainLooper();
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("You already have a scooter booked. Please return this before making a new booking"));
            }
        });
    }

    @Test
    public void viewMapClickedNoCurrentBookingChangesActivity() throws Exception
    {
        Button viewMapBtn = (Button) activity.findViewById(R.id.view_map_btn);
        //Customer ID 92 does not have a booking
        activity.CustomerID = "92";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                viewMapBtn.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent expectedIntent = new Intent(activity, ScooterMap.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }

//    @Test todo: when double booking preventing is added
//    public void viewMapCurrentBookingShowsToast() throws Exception
//    {
//        Button viewMapBtn = (Button) activity.findViewById(R.id.view_map_btn);
//        //Customer ID 104 does not have a booking
//        activity.CustomerID = "104";
//        activity.runOnUiThread(new Runnable() {
//            public void run() {
//                viewMapBtn.performClick();
//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                ShadowLooper.idleMainLooper();
//                assertEquals(ShadowToast.getTextOfLatestToast().toString(), (todo:addtoast ));
//
//            }
//        });
//    }

    @Test
    public void viewBookingWithCurrentBookingChangesActivity() throws Exception
    {
        Button viewBookingsBtn = (Button) activity.findViewById(R.id.view_booking_btn);
        //Customer ID 104 has a booking
        activity.CustomerID = "104";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                viewBookingsBtn.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                Intent expectedIntent = new Intent(activity, ViewBooking.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());


            }
        });
    }

    @Test
    public void viewBookingWithNoBookingShowsToast() throws Exception
    {
        Button viewBookingsBtn = (Button) activity.findViewById(R.id.view_booking_btn);
        //Customer ID 92 has no booking
        activity.CustomerID = "92";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                viewBookingsBtn.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ShadowLooper.idleMainLooper();
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("You do not currently have a scooter booked. Please make a new booking through the map or enter a code"));

            }
        });
    }

    @Test
    public void sendFeedbackButtonChangesActivity() throws Exception
    {
        Button sendFeedbackBtn = (Button) activity.findViewById(R.id.send_feeback_btn);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                sendFeedbackBtn.performClick();

                Intent expectedIntent = new Intent(activity, SendFeedback.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }

    @Test
    public void addCardNoExistingCardChangesActivity() throws Exception
    {
        Button  addCardBtn = (Button) activity.findViewById(R.id.add_card_btn);
        //Customer ID 104 does not have a card associated with the account
        activity.CustomerID = "104";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                addCardBtn.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent expectedIntent = new Intent(activity, AddCard.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());

            }
        });
    }

    @Test
    public void addCardExistingCardShowsToast() throws Exception
    {
        Button  addCardBtn = (Button) activity.findViewById(R.id.add_card_btn);
        //Customer ID 92 does have a card associated with the account
        activity.CustomerID = "112";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                addCardBtn.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ShadowLooper.idleMainLooper();
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("You already have a card assigned to this account"));


            }
        });
    }

    @Test
    public void logOutChangesActivity() throws Exception
    {
        Button logOutBtn = (Button) activity.findViewById(R.id.log_out_btn);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                logOutBtn.performClick();

                Intent expectedIntent = new Intent(activity, MainActivity.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }




}