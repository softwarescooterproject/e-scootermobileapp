package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.test.core.app.ApplicationProvider;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class LogInTest extends LogIn{

    private LogIn activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(LogIn.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void accountFoundMethodTrueChangesActivity(){
        activity.accountFound(true);

        Intent expectedIntent = new Intent(activity, MainMenu.class);
        Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

    @Test
    public void accountFoundMethodFalseShowsToast(){
        activity.accountFound(false);
        ShadowLooper.idleMainLooper();
        assertEquals("Invalid email and password combination.", ShadowToast.getTextOfLatestToast().toString() );
    }

    @Test
    public void EditTextRejectsEmptyText(){
        Button logInButton = (Button) activity.findViewById(R.id.confirm_log_in_btn);
        final EditText email = (EditText) activity.findViewById((R.id.editTextEmailLogIn));
        final EditText password = (EditText) activity.findViewById((R.id.editTextPasswordLogIn));

        activity.runOnUiThread(new Runnable() {
            public void run() {
                email.setText("");
                password.setText("test");
                logInButton.performClick();
                //Check that toast was shown
                ShadowLooper.idleMainLooper(Toast.LENGTH_LONG);
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Both email and password must not be empty"));

            }
        });
    }

    @Test
    public void invalidDetailsReturnAccountNotFound(){
        Button logInButton = (Button) activity.findViewById(R.id.confirm_log_in_btn);
        final EditText email = (EditText) activity.findViewById((R.id.editTextEmailLogIn));
        final EditText password = (EditText) activity.findViewById((R.id.editTextPasswordLogIn));

        activity.runOnUiThread(new Runnable() {
            public void run() {
                email.setText("invalidEmail");
                password.setText("invalidPassword");
                logInButton.performClick();

                //Sleep required to give program time to query API
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Check that toast was shown
                ShadowLooper.idleMainLooper(1);
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Invalid email and password combination."));
            }
        });
    }

    @Test
    public void validDetailsChangeActivity(){
        Button logInButton = (Button) activity.findViewById(R.id.confirm_log_in_btn);
        final EditText email = (EditText) activity.findViewById((R.id.editTextEmailLogIn));
        final EditText password = (EditText) activity.findViewById((R.id.editTextPasswordLogIn));

        activity.runOnUiThread(new Runnable() {
            public void run() {
                //This data exists in the database
                email.setText("3test@test.com");
                password.setText("test");
                logInButton.performClick();

                //Sleep required to give program time to query API
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent expectedIntent = new Intent(activity, MainMenu.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });

    }
}