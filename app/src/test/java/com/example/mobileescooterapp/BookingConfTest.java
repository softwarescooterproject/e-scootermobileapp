package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class BookingConfTest extends BookingConf {

    private BookingConf activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(BookingConf.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void buttonClickChangeActivity() {
        Button continue_btn = (Button) activity.findViewById(R.id.continue_bookingconf_btn); //Initialise the button
        activity.runOnUiThread(new Runnable() {
            public void run() {
                continue_btn.performClick();
                Intent expectedIntent = new Intent(activity, MainMenu.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }

}