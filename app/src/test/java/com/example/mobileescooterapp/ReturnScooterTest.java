package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class ReturnScooterTest extends ReturnScooter{

    private ReturnScooter activity;


    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(ReturnScooter.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void scooterReturnedTrueChangesActivity(){
        activity.scooterReturned(true);
        Intent expectedIntent = new Intent(activity, MainMenu.class);
        Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

    @Test
    public void scooterReturnedMethodFalseShowsToast(){
        activity.scooterReturned(false);
        ShadowLooper.idleMainLooper();
        assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Return failed"));
    }


}