package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class ViewBookingTest {

    private ViewBooking activity;

    @Before
    public void setUp() throws Exception{
        //todo need to add something to parse in intents/extras to make it work
        activity = Robolectric.buildActivity(ViewBooking.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void extendDurationButtonChangesActivity() throws Exception
    {
        Button  extendDurationBtn = (Button) activity.findViewById(R.id.extend_duration_btn);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                extendDurationBtn.performClick();

                Intent expectedIntent = new Intent(activity, ExtendBooking.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }

    @Test
    public void cancelBookingButtonChangesActivity() throws Exception
    {
        Button returnScooterBtn = (Button) activity.findViewById(R.id.returnScooterBtn);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                returnScooterBtn.performClick();

                Intent expectedIntent = new Intent(activity, ReturnScooter.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }

}