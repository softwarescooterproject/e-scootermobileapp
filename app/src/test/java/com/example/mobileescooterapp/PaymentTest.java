package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class PaymentTest {

    private Payment activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(Payment.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void emptyFieldsShowsToast(){

        Button payBtn = (Button) activity.findViewById(R.id.pay_Btn);
        payBtn.performClick();
        ShadowLooper.idleMainLooper();
        assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("All fields must be filled"));

    }

    @Test
    public void paidMethodTrueChangesActivity(){
        activity.paid(true);
        Intent expectedIntent = new Intent(activity, BookingConf.class);
        Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

    @Test
    public void paidMethodFalseShowsToast(){
        activity.paid(false);
        ShadowLooper.idleMainLooper();
        assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Payment not successful"));
    }



    @Test
    public void checkForDiscountEligibleAppliesDiscount() throws Exception
    {
        Button discountApply = (Button) activity.findViewById((R.id.apply_discount_btn));
        //Customer ID 104 has no booking
        activity.CustomerID = "104";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                discountApply.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ShadowLooper.idleMainLooper();
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Discount Applied"));

            }
        });
    }

    @Test
    public void checkForDiscountNotEligibleShowsToast() throws Exception
    {
        Button discountApply = (Button) activity.findViewById((R.id.apply_discount_btn));
        //Customer ID 104 has no booking
        activity.CustomerID = "92";
        activity.runOnUiThread(new Runnable() {
            public void run() {
                discountApply.performClick();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ShadowLooper.idleMainLooper();
                assertEquals(ShadowToast.getTextOfLatestToast().toString(), ("Not Eligible for Discount"));

            }
        });
    }


}