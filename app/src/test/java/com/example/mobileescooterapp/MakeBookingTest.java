package com.example.mobileescooterapp;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

import android.app.Application;
import android.content.Intent;
import android.widget.Button;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class MakeBookingTest extends MakeBooking {

    private MakeBooking activity;

    @Before
    public void setUp() throws Exception{
        activity = Robolectric.buildActivity(MakeBooking.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void checkoutChangesActivity() throws Exception
    {
        Button checkoutBtn = (Button) activity.findViewById(R.id.checkout_btn);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                checkoutBtn.performClick();

                Intent expectedIntent = new Intent(activity, Payment.class);
                Intent actual = shadowOf((Application) ApplicationProvider.getApplicationContext()).getNextStartedActivity();
                assertEquals(expectedIntent.getComponent(), actual.getComponent());
            }
        });
    }
}