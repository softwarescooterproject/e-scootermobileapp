package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class AccountCreated extends AppCompatActivity {

    Button continue_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_created);

        continue_btn = (Button) findViewById(R.id.created_continue_btn);
        continue_btn.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(AccountCreated.this, LogIn.class);
            startActivity(intent);
        });
    }
}