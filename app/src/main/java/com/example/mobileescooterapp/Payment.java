package com.example.mobileescooterapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Payment extends AppCompatActivity {

    Button payBtn;
    Button useExistingCardBtn;
    Button checkoutBtn;
    Button discountApply;
    TextView discount_applied;
    TextView final_price;
    EditText cardNumberTxt;
    EditText expiryDateTxt;
    EditText cvcTxt;
    String CustomerID;
    String FirstName;
    String ScooterID;

    String cardNumber;
    String expiryDate;
    String cvc;
    Boolean cardDetails;

    String price;
    int discountAllowed;


    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    String costID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");
        costID = i.getStringExtra("costID");
        ScooterID = i.getStringExtra("ScooterID");
        cardNumber = i.getStringExtra("cardNumber");
        expiryDate = i.getStringExtra("expiryDate");
        cardDetails = i.getBooleanExtra("cardDetails", false);
        price = i.getStringExtra("price");

        payBtn = (Button) findViewById(R.id.pay_Btn);   //Initialise the button
        cardNumberTxt = (EditText) findViewById(R.id.cardNumberBooking);
        expiryDateTxt = (EditText) findViewById(R.id.expiryDateBooking);
        cvcTxt = (EditText) findViewById(R.id.CvcEditText);

        useExistingCardBtn = (Button) findViewById(R.id.use_existing_card_btn);
        discountApply = (Button) findViewById((R.id.apply_discount_btn));
        discount_applied = (TextView) findViewById(R.id.discount_applied);
        discount_applied.setVisibility(View.INVISIBLE);
        final_price = (TextView) findViewById(R.id.final_price_txt);

        final_price.setText((getResources().getString(R.string.final_price) + " £" + price));

        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                    String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 4; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 6) {
                        clean = clean + yyyymm.substring(clean.length());
                    } else {
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        int year = Integer.parseInt(clean.substring(0, 4));
                        int mon = Integer.parseInt(clean.substring(4, 6));

                        mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                        cal.set(Calendar.MONTH, mon - 1);
                        year = (year < 2022) ? 2022 : (year > 2100) ? 2100 : year;
                        cal.set(Calendar.YEAR, year);
                        // ^ first set year for the line below to work correctly
                        //with leap years - otherwise, date e.g. 29/02/2012
                        //would be automatically corrected to 28/02/2012

                        clean = String.format("%02d%02d", year, mon);
                    }

                    clean = String.format("%s-%s", clean.substring(0, 4),
                            clean.substring(4, 6));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    expiryDateTxt.setText(current);
                    expiryDateTxt.setSelection(sel < current.length() ? sel : current.length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

            private String current = "";
            private String yyyymm = "YYYYMM";
            private Calendar cal = Calendar.getInstance();
        };

        expiryDateTxt.addTextChangedListener(tw);

        useExistingCardBtn.setOnClickListener(v -> {

            if (cardNumber == null && expiryDate == null && cvc == null) {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "No card is saved to this account", Toast.LENGTH_LONG).show());

            } else {
                cardNumberTxt.setText(cardNumber);
                expiryDateTxt.setText(expiryDate);
            }
        });

        payBtn.setOnClickListener(v -> {   //Set on click listener of button
            cardNumber = cardNumberTxt.getText().toString();
            expiryDate = expiryDateTxt.getText().toString();
            cvc = cvcTxt.getText().toString();

            if (!cardNumber.isEmpty() && !expiryDate.isEmpty() && !cvc.isEmpty()) {
                try {
                    makeBooking(cardNumber, expiryDate, cvc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "All fields must be filled", Toast.LENGTH_LONG).show());
            }
        });

        discountApply.setOnClickListener(v -> {
            checkDiscount();

        });
    }

    // Shows toast if unsuccessful, moves to BookingConf activity if it was.
    protected void paid(Boolean found) {
        if (found) {
            Intent intent = new Intent(Payment.this, BookingConf.class);
            intent.putExtra("CustomerID", CustomerID);
            intent.putExtra("First Name", FirstName);
            intent.putExtra("costID", costID);
            startActivity(intent);
        } else {
            //Need to run on UI thread to avoid errors
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Payment not successful", Toast.LENGTH_LONG).show());
        }
    }

    // Runs call on API to addNewSession
    private void makeBooking(String cardNumber, String expiryDate, String cvv) throws Exception {

        String json = "{\"CustomerID\": \"" + CustomerID +
                "\", \"ScooterID\": \"" + ScooterID +
                "\", \"CostID\": \"" + costID + "\"}";
        RequestBody requestBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/addNewSession")
                .addHeader("Content-Type", "application/json")
                .post(requestBody)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = response.body().string();
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Booking made successfully", Toast.LENGTH_LONG).show());
//                runOnUiThread(() -> Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show());
                paid(true);
            } catch (IOException e) {
                paid(false);
            }
        }).start();
    }

    // Runs call on API to /getDiscountEligibility/'CustomerID'
    private void checkDiscount() {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getDiscountEligibility/" + CustomerID)
                .get()
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = response.body().string();
                if (str.equals("1\n")) {
                    discountAllowed = 1;
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Discount Applied", Toast.LENGTH_LONG).show());
                    double newPrice = Double.parseDouble(price);
                    newPrice = round(newPrice * 0.75, 2);
                    String newPriceStr = String.valueOf(newPrice);
                    final_price.post(new Runnable() {
                        public void run() {
                            final_price.setText((getResources().getString(R.string.final_price) + " £" + newPriceStr));
                        }
                    });
                }
                if (str.equals("0\n")) {
                    discountAllowed = 0;
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Not Eligible for Discount", Toast.LENGTH_LONG).show());
                }
            } catch (IOException e) {
            }
        }).start();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
