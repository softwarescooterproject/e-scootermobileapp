package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import android.content.Intent;
import android.view.View;
import android.view.textclassifier.TextLinks;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.mobileescooterapp.databinding.ActivityMapBinding;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ScooterMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapBinding binding;
    Button book_btn;
    EditText scooter_id;
    Spinner location_menu;
    String CustomerID;
    String FirstName;
    String scootID;
    Boolean scootersFound = false;
    Boolean costFound;
    Boolean currentSession;
    int totalAvailable = 0;
    int totalLoc1 = 0;
    int totalLoc2 = 0;
    int totalLoc3 = 0;
    int totalLoc4 = 0;
    int totalLoc5 = 0;
    MarkerOptions leedsHospitalMarker;
    MarkerOptions leedsEdgeMarker;
    MarkerOptions leedsTrinityMarker;
    MarkerOptions leedsStationMarker;
    MarkerOptions leedsMerrionMarker;
    List<String> locations;
    int locationID = 0;
    int scooterID;

    String cardNumber;
    String expiryDate;
    String cvc;
    Boolean cardDetails = false;

    String price1;
    String price2;
    String price3;
    String price4;

    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        locations = new ArrayList<String>();
        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");

        binding = ActivityMapBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Bundle extras = i.getExtras();
        if (extras == null) { //for testing purposes - will need to be updated if booking expires
            CustomerID = "92";
            locationID = 1;
            currentSession = false;
        }

        getCardDetails();
        getPrice();
        doesCustomerHaveSessionAlready();


        book_btn = (Button) findViewById(R.id.makeMapBooking);//Initialise the button
        book_btn.setOnClickListener(v -> {
            while (cardDetails == null && costFound != true && currentSession == null) {
            }

            if (currentSession == true) {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "You already have a scooter booked. Please return this before making a new booking", Toast.LENGTH_LONG).show());
            } else {
                getScooterID();
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fillDropDown();
        location_menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                String item = parent.getItemAtPosition(i).toString();
                if (item == "Trinity Centre") {
                    locationID = 1;
                } else if (item == "Train Station") {
                    locationID = 2;
                } else if (item == "Merrion Centre") {
                    locationID = 3;
                } else if (item == "Leeds Royal Infirmary") {
                    locationID = 4;
                } else if (item == "The Edge") {
                    locationID = 5;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void doesCustomerHaveSessionAlready() {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getCurrentSession/" + CustomerID)
                .build();

        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = Objects.requireNonNull(response.body()).string();
                if (str.equals("\"[]\"\n")) {
                    currentSession = false;
                } else {
                    currentSession = true;
                }
            } catch (IOException e) {

            }
        }).start();
    }

    public void fillDropDown() {

        String total1;
        String total2;
        String total3;
        String total4;
        String total5;

        Intent i = getIntent();
        Bundle extras = i.getExtras();
        if (extras == null) { //for testing purposes - will need to be updated if booking expires
            CustomerID = "92";
            locationID = 1;
            total1 = "1";
            total2 = "1";
            total3 = "1";
            total4 = "1";
            total5 = "1";
        } else {
            total1 = i.getStringExtra("Total Available 1");
            total2 = i.getStringExtra("Total Available 2");
            total3 = i.getStringExtra("Total Available 3");
            total4 = i.getStringExtra("Total Available 4");
            total5 = i.getStringExtra("Total Available 5");
        }

        totalLoc1 = Integer.parseInt(total1);
        totalLoc2 = Integer.parseInt(total2);
        totalLoc3 = Integer.parseInt(total3);
        totalLoc4 = Integer.parseInt(total4);
        totalLoc5 = Integer.parseInt(total5);

        if (totalLoc1 != 0) {
            locations.add("Trinity Centre");
        }
        if (totalLoc2 != 0) {
            locations.add("Train Station");
        }
        if (totalLoc3 != 0) {
            locations.add("Merrion Centre");
        }
        if (totalLoc4 != 0) {
            locations.add("Leeds Royal Infirmary");
        }
        if (totalLoc5 != 0) {
            locations.add("The Edge");
        }

        location_menu = (Spinner) findViewById(R.id.dropdown_locations);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, locations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        location_menu.setAdapter(adapter);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        BitmapDescriptor redMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
        BitmapDescriptor greenMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);


        LatLng merrionCentre = new LatLng(53.802126, -1.543302);
        if (totalLoc3 == 0) {
            leedsMerrionMarker = new MarkerOptions()
                    .position(merrionCentre)
                    .title("Merrion Centre")
                    .snippet("Available Scooters:" + totalLoc3)
                    .icon(redMarker);
        } else {
            leedsMerrionMarker = new MarkerOptions()
                    .position(merrionCentre)
                    .title("Merrion Centre")
                    .snippet("Available Scooters:" + totalLoc3)
                    .icon(greenMarker);
        }
        mMap.addMarker(leedsMerrionMarker);

        LatLng trainStation = new LatLng(53.794829, -1.547601);
        if (totalLoc2 == 0) {
            leedsStationMarker = new MarkerOptions()
                    .position(trainStation)
                    .title("Train Station")
                    .snippet("Available Scooters:" + totalLoc2)
                    .icon(redMarker);
        } else {
            leedsStationMarker = new MarkerOptions()
                    .position(trainStation)
                    .title("Train Station")
                    .snippet("Available Scooters:" + totalLoc2)
                    .icon(greenMarker);
        }
        mMap.addMarker(leedsStationMarker);

        LatLng trinityCentre = new LatLng(53.796628, -1.5445);
        if (totalLoc1 == 0) {
            leedsTrinityMarker = new MarkerOptions()
                    .position(trinityCentre)
                    .title("Trinity Centre")
                    .snippet("Available Scooters:" + totalLoc1)
                    .icon(redMarker);
        } else {
            leedsTrinityMarker = new MarkerOptions()
                    .position(trinityCentre)
                    .title("Trinity Centre")
                    .snippet("Available Scooters:" + totalLoc1)
                    .icon(greenMarker);
        }
        mMap.addMarker(leedsTrinityMarker);

        LatLng LRIHospital = new LatLng(53.802911, -1.551571);
        if (totalLoc4 == 0) {
            leedsHospitalMarker = new MarkerOptions()
                    .position(LRIHospital)
                    .title("Leeds Royal Infirmary")
                    .snippet("Available Scooters:" + totalLoc4)
                    .icon(redMarker);
        } else {
            leedsHospitalMarker = new MarkerOptions()
                    .position(LRIHospital)
                    .title("Leeds Royal Infirmary")
                    .snippet("Available Scooters:" + totalLoc4)
                    .icon(greenMarker);
        }
        mMap.addMarker(leedsHospitalMarker);

        LatLng edgeCentre = new LatLng(53.804181, -1.553221);
        if (totalLoc5 == 0) {
            leedsEdgeMarker = new MarkerOptions()
                    .position(edgeCentre)
                    .title("University of Leeds Edge Sports Centre")
                    .snippet("Available Scooters:" + totalLoc5)
                    .icon(redMarker);
        } else {
            leedsEdgeMarker = new MarkerOptions()
                    .position(edgeCentre)
                    .title("The Edge Sports Centre")
                    .snippet("Available Scooters:" + totalLoc5)
                    .icon(greenMarker);
        }
        mMap.addMarker(leedsEdgeMarker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(edgeCentre));
    }

    // Runs call on API to getScooterDetails/all
    public void getScooterID() {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getScooterDetails/all")
                .build();

        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = Objects.requireNonNull(response.body()).string();
                JSONArray jObject = new JSONArray(str);
                int available = 0;
                int currentLocation = 0;
                for (int i = 0; i < jObject.length(); i++) {
                    currentLocation = jObject.getJSONObject(i).getInt("CurrentLocation");
                    available = jObject.getJSONObject(i).getInt("Available");
                    scooterID = jObject.getJSONObject(i).getInt("ScooterID");
                    if ((currentLocation == locationID) && (available == 1)) {
                        scootersFound = true;
                        Intent intent = new Intent(ScooterMap.this, MakeBooking.class); //Intent(thisActivity.this, destinationActivity.class)
                        intent.putExtra("CustomerID", CustomerID);
                        intent.putExtra("First Name", FirstName);
                        scootID = String.valueOf(scooterID);
                        intent.putExtra("ScooterID", scootID);
                        intent.putExtra("cardNumber", cardNumber);
                        intent.putExtra("expiryDate", expiryDate);
                        intent.putExtra("cvc", cvc);
                        intent.putExtra("cardDetails", cardDetails);
                        intent.putExtra("price1", price1);
                        intent.putExtra("price2", price2);
                        intent.putExtra("price3", price3);
                        intent.putExtra("price4", price4);
                        startActivity(intent);  //Go to the new activity
                    }
                }

            } catch (IOException | JSONException e) {
                scootersFound = false;
            }
        }).start();
    }

    // Runs call on API to getCardDetails/'CustomerID'
    private void getCardDetails() {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getCardDetails/" + CustomerID)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = response.body().string();

                JSONArray jObject = new JSONArray(str);
                cardNumber = jObject.getJSONObject(0).getString("Card Number");
                expiryDate = jObject.getJSONObject(0).getString("Expiry Date");
                cvc = jObject.getJSONObject(0).getString("CVC");
                cardDetails = true;
            } catch (IOException | JSONException e) {
                cardDetails = false;
            }
        }).start();
    }

    // Runs call on API to getScooterPricing/all
    public void getPrice() {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getScooterPricing/all ")
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                int costId = 0;
                int duration = 0;
                int price = 0;
                String str = response.body().string();
                JSONArray jObject = new JSONArray(str);
                for (int i = 0; i < jObject.length(); i++) {
                    costId = jObject.getJSONObject(i).getInt("CostID");
                    duration = jObject.getJSONObject(i).getInt("Duration(mins)");
                    price = jObject.getJSONObject(i).getInt("Price");
                    if (costId == 1) {
                        price1 = Integer.toString(price);
                    }
                    if (costId == 2) {
                        price2 = Integer.toString(price);
                    }
                    if (costId == 3) {
                        price3 = Integer.toString(price);
                    }
                    if (costId == 4) {
                        price4 = Integer.toString(price);
                    }
                }
                costFound = true;
            } catch (IOException | JSONException e) {
                costFound = false;
            }
        }).start();

    }

}

