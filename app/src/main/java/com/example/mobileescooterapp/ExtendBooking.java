package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ExtendBooking extends AppCompatActivity {

    Spinner extension_menu;
    List<String> extensions;
    int costID = 0;
    String costIDStr;
    String scootID;
    String customerID;
    String firstName;

    Button confirm_btn;
    Boolean sessionExtended = false;
    String scootIDstr;
    int availability;

    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extend_booking);

        Intent i = getIntent();
        customerID = i.getStringExtra("CustomerID");
        firstName = i.getStringExtra("First Name");
        scootID = i.getStringExtra("ScooterID");
        costIDStr = i.getStringExtra("CostID");

        Bundle extras = i.getExtras();
        if (extras == null) { //for testing purposes - will need to be updated if booking expires
            costIDStr = "3";
        }
        costID = Integer.parseInt(costIDStr);

        extensions = new ArrayList<String>();
        fillDropDown();
        extension_menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                String item = parent.getItemAtPosition(i).toString();
                if (item == "1 hour") {
                    costID = 1;
                } else if (item == "4 hours") {
                    costID = 2;
                } else if (item == "1 day") {
                    costID = 3;
                } else if (item == "1 week") {
                    costID = 4;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        confirm_btn = (Button) findViewById(R.id.confirm_extend_btn);
        confirm_btn.setOnClickListener(v -> {   //Set on click listener of button
            confirmExtension();

        });

    }

    public void fillDropDown() {

        if (costID == 1) {
            extensions.add("4 hours");
            extensions.add("1 day");
            extensions.add("1 week");
        } else if (costID == 2) {
            extensions.add("1 day");
            extensions.add("1 week");
        } else if (costID == 3) {
            extensions.add("1 week");
        } else if (costID == 4) {
            extensions.add("No extension available");
        }

        extension_menu = (Spinner) findViewById(R.id.durationSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, extensions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        extension_menu.setAdapter(adapter);
    }

    // Shows toast if unsuccessful, moves to MainMenu activity if it was.
    protected void sessionExtended(boolean success) {
        if (success) {
            Intent intent = new Intent(ExtendBooking.this, MainMenu.class); //Intent(thisActivity.this, destinationActivity.class)
            intent.putExtra("CustomerID", customerID);
            intent.putExtra("First Name", firstName);
            startActivity(intent);  //Go to the new activity
        } else {
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Extension failed", Toast.LENGTH_LONG).show());
        }
    }

    // Runs call on API to extendSession
    public void confirmExtension() {
        String json = "{\"CustomerID\": \"" + customerID +
                "\", \"ScooterID\": \"" + scootID +
                "\", \"CostID\": \"" + costID + "\" }";
        RequestBody requestBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/extendSession")
                .addHeader("Content-Type", "application/json")
                .put(requestBody)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Booking extension successful!", Toast.LENGTH_LONG).show());
                sessionExtended(true);
            } catch (IOException e) {
                sessionExtended(false);
            }
        }).start();

    }


}

