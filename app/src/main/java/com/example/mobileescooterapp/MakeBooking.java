package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.json.JSONArray;
import org.json.JSONException;

public class MakeBooking extends AppCompatActivity {


    Button checkoutBtn;
    TextView scooterID_txt;
    TextView price_txt;
    String CustomerID;
    String FirstName;
    String ScooterID;
    Boolean costFound;
    int price;

    String cardNumber;
    String expiryDate;
    Boolean cardDetails;

    String price1;
    String price2;
    String price3;
    String price4;

    Spinner location_menu;
    List<String> locations;

    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    int costID = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_booking);

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");
        ScooterID = i.getStringExtra("ScooterID");
        cardNumber = i.getStringExtra("cardNumber");
        expiryDate = i.getStringExtra("expiryDate");
        cardDetails = i.getBooleanExtra("cardDetails", false);
        price1 = i.getStringExtra("price1");
        price2 = i.getStringExtra("price2");
        price3 = i.getStringExtra("price3");
        price4 = i.getStringExtra("price4");

        Bundle extras = i.getExtras();
        if (extras == null) { //for testing purposes
            CustomerID = "104";
            price1 = "10";
            price2 = "35";
            price3 = "180";
            price4 = "400";
        }

        checkoutBtn = (Button) findViewById(R.id.checkout_btn);

        // TODO: make textviews display scooter id and price of session before discount
        scooterID_txt = (TextView) findViewById((R.id.scooter_ID));
        scooterID_txt.setText("Scooter ID: " + ScooterID);

        price_txt = (TextView) findViewById((R.id.price_textview));

        locations = new ArrayList<String>();
        fillDropDown();
        location_menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                String item = parent.getItemAtPosition(i).toString();
                if (item == "1 hour") {
                    costID = 1;
                    price_txt.setText("£" + price1);
                    price = Integer.valueOf(price1);
                } else if (item == "4 hours") {
                    costID = 2;
                    price_txt.setText("£" + price2);
                    price = Integer.valueOf(price2);
                } else if (item == "1 day") {
                    costID = 3;
                    price_txt.setText("£" + price3);
                    price = Integer.valueOf(price3);
                } else if (item == "1 week") {
                    costID = 4;
                    price_txt.setText("£" + price4);
                    price = Integer.valueOf(price4);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkoutBtn.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(MakeBooking.this, Payment.class); //Intent(thisActivity.this, destinationActivity.class)
            intent.putExtra("CustomerID", CustomerID);
            intent.putExtra("First Name", FirstName);
            intent.putExtra("ScooterID", ScooterID);
            intent.putExtra("cardNumber", cardNumber);
            intent.putExtra("expiryDate", expiryDate);
            String costIDStr = Integer.toString(costID);
            intent.putExtra("costID", costIDStr);
            intent.putExtra("cardDetails", cardDetails);
            intent.putExtra("price", Integer.toString(price));
            startActivity(intent);  //Go to the new activity
        });
    }

    public void fillDropDown() {
        locations.add("1 hour");
        locations.add("4 hours");
        locations.add("1 day");
        locations.add("1 week");
        location_menu = (Spinner) findViewById(R.id.booking_length_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, locations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        location_menu.setAdapter(adapter);
    }
}


