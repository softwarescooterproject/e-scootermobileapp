package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainMenu extends AppCompatActivity {


    Button bookScooterBtn;
    Button viewMapBtn;
    Button viewBookingsBtn;
    Button sendFeedbackBtn;
    Button logOutBtn;
    Button addCardBtn;
    TextView welcometxt;
    String CustomerID;
    String FirstName;
    int totalAvailable = 0;
    int totalLoc1 = 0;
    int totalLoc2 = 0;
    int totalLoc3 = 0;
    int totalLoc4 = 0;
    int totalLoc5 = 0;
    Boolean scootersFound = false;


    String cardNumber;
    String expiryDate;
    Boolean cardDetails = false;

    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");

        welcometxt = (TextView) findViewById(R.id.welcome);
        welcometxt.setText("Welcome back " + FirstName);

        getScoots();

        bookScooterBtn = (Button) findViewById(R.id.book_scooter_btn);   //Initialise the button
        bookScooterBtn.setOnClickListener(v -> {   //Set on click listener of button
            doesCustomerHaveSessionAlready(CustomerID);
        });

        viewMapBtn = (Button) findViewById(R.id.view_map_btn);   //Initialise the button
        viewMapBtn.setOnClickListener(v -> {   //Set on click listener of button
//            TODO:prevent double booking from map;
            Intent intent = new Intent(MainMenu.this, ScooterMap.class); //Intent(thisActivity.this, destinationActivity.class)
            intent.putExtra("CustomerID", CustomerID);
            intent.putExtra("First Name", FirstName);
            String tot1 = Integer.toString(totalLoc1);
            intent.putExtra("Total Available 1", tot1);
            String tot2 = Integer.toString(totalLoc2);
            intent.putExtra("Total Available 2", tot2);
            String tot3 = Integer.toString(totalLoc3);
            intent.putExtra("Total Available 3", tot3);
            String tot4 = Integer.toString(totalLoc4);
            intent.putExtra("Total Available 4", tot4);
            String tot5 = Integer.toString(totalLoc5);
            intent.putExtra("Total Available 5", tot5);
            startActivity(intent);  //Go to the new activity
        });

        viewBookingsBtn = (Button) findViewById(R.id.view_booking_btn);   //Initialise the button
        viewBookingsBtn.setOnClickListener(v -> {   //Set on click listener of button
            Request request = new Request.Builder()
                    .url("https://e-scooter-api.herokuapp.com/getCurrentSession/" + CustomerID)
                    .build();

            //Have to run httpClient call on different thread
            new Thread(() -> {
                try (Response response = httpClient.newCall(request).execute()) {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);

                    String str = response.body().string();
                    str = str.replace("\\", "");
                    str = str.substring(1, str.length() - 1);
                    JSONArray jObject = new JSONArray(str);
                    String costID = jObject.getJSONObject(0).getString("CostID");
                    String dateHired = jObject.getJSONObject(0).getString("DateHired");
                    Double finalCost = jObject.getJSONObject(0).getDouble("FinalCost");
                    int paid = jObject.getJSONObject(0).getInt("Paid");
                    int returned = jObject.getJSONObject(0).getInt("Returned");
                    String scooterID = jObject.getJSONObject(0).getString("ScooterID");
                    int sessionID = jObject.getJSONObject(0).getInt("SessionID");
                    String timeHired = jObject.getJSONObject(0).getString("TimeHired");

                    Intent intent = new Intent(MainMenu.this, ViewBooking.class); //Intent(thisActivity.this, destinationActivity.class)
                    intent.putExtra("CustomerID", CustomerID);
                    intent.putExtra("First Name", FirstName);
                    intent.putExtra("costID", costID);
                    intent.putExtra("scooterID", scooterID);
                    intent.putExtra("timeHired", timeHired);
                    intent.putExtra("dateHired", dateHired);

                    startActivity(intent);  //Go to the new activity

                } catch (IOException | JSONException e) {
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "You do not currently have a scooter booked. Please make a new booking through the map or enter a code", Toast.LENGTH_LONG).show());
                }
            }).start();

        });

        sendFeedbackBtn = (Button) findViewById(R.id.send_feeback_btn);   //Initialise the button
        sendFeedbackBtn.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(MainMenu.this, SendFeedback.class); //Intent(thisActivity.this, destinationActivity.class)
            intent.putExtra("CustomerID", CustomerID);
            intent.putExtra("First Name", FirstName);
            startActivity(intent);  //Go to the new activity
        });


        addCardBtn = (Button) findViewById(R.id.add_card_btn);   //Initialise the button
        addCardBtn.setOnClickListener(v -> {   //Set on click listener of button
            doesCustomerHaveCardAlready(CustomerID);
        });


        logOutBtn = (Button) findViewById(R.id.log_out_btn);   //Initialise the button
        logOutBtn.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(MainMenu.this, MainActivity.class); //Intent(thisActivity.this, destinationActivity.class)
            startActivity(intent);  //Go to the new activity
        });
    }

    // Runs call on API to getCardDetails/'CustomerID'
    private void doesCustomerHaveCardAlready(String customerId) {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getCardDetails/" + CustomerID)
                .build();

        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = Objects.requireNonNull(response.body()).string();
                if (str.equals("\"No card on account\"\n")) {
                    Intent intent = new Intent(MainMenu.this, AddCard.class); //Intent(thisActivity.this, destinationActivity.class)
                    intent.putExtra("CustomerId", CustomerID);
                    intent.putExtra("First Name", FirstName);
                    startActivity(intent);  //Go to the new activity
                } else {
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "You already have a card assigned to this account", Toast.LENGTH_LONG).show());
                }
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), (CharSequence) e, Toast.LENGTH_LONG).show());

            }
        }).start();
    }

    // Runs call on API to /getCurrentSession/'CustomerID'
    protected void doesCustomerHaveSessionAlready(String customerId) {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getCurrentSession/" + CustomerID)
                .build();

        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = Objects.requireNonNull(response.body()).string();
                if (str.equals("\"[]\"\n")) {
                    Intent intent = new Intent(MainMenu.this, EnterCode.class); //Intent(thisActivity.this, destinationActivity.class)
                    intent.putExtra("CustomerID", CustomerID);
                    intent.putExtra("First Name", FirstName);
                    startActivity(intent);  //Go to the new activity
                } else {
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "You already have a scooter booked. Please return this before making a new booking", Toast.LENGTH_LONG).show());
                }
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), (CharSequence) e, Toast.LENGTH_LONG).show());

            }
        }).start();
    }

    // Runs call on API to /getScooterDetails/all
    public void getScoots() {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getScooterDetails/all")
                .build();

        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = Objects.requireNonNull(response.body()).string();
                JSONArray jObject = new JSONArray(str);
                int available = 0;
                int currentLocation = 0;
                for (int i = 0; i < jObject.length(); i++) {
                    currentLocation = jObject.getJSONObject(i).getInt("CurrentLocation");
                    available = jObject.getJSONObject(i).getInt("Available");
                    if ((currentLocation == 1) && (available == 1)) {
                        totalLoc1 = totalLoc1 + 1;
                    }
                    if ((currentLocation == 2) && (available == 1)) {
                        totalLoc2 = totalLoc2 + 1;
                    }
                    if ((currentLocation == 3) && (available == 1)) {
                        totalLoc3 = totalLoc3 + 1;
                    }
                    if ((currentLocation == 4) && (available == 1)) {
                        totalLoc4 = totalLoc4 + 1;
                    }
                    if ((currentLocation == 5) && (available == 1)) {
                        totalLoc5 = totalLoc5 + 1;
                    }
                }
                scootersFound = true;
            } catch (IOException | JSONException e) {
                scootersFound = false;
            }
        }).start();
    }


}