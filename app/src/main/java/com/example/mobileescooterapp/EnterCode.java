package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.*;


public class EnterCode extends AppCompatActivity {

    Button confirm_btn;
    EditText scooterID;
    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    boolean accountFound = false;

    String CustomerID;
    String FirstName;
    String scooterIDstr;

    String cardNumber;
    String expiryDate;
    Boolean cardDetails = true;

    String price1;
    String price2;
    String price3;
    String price4;
    Boolean costFound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_code);

        scooterID = (EditText) findViewById(R.id.ScooterIDText);
        confirm_btn = (Button) findViewById(R.id.go_to_booking_btn);

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");

        getPrice();

        confirm_btn.setOnClickListener(v -> {   //Set on click listener of button
            scooterIDstr = scooterID.getText().toString();

            if (!scooterIDstr.isEmpty()) {
                try {
                    enterCode(scooterIDstr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // Moves to MakeBooking activity and populates all extras for the intent
    private void nextActivity() {

        Intent intent = new Intent(EnterCode.this, MakeBooking.class);
        intent.putExtra("CustomerID", CustomerID);
        intent.putExtra("First Name", FirstName);
        intent.putExtra("ScooterID", scooterIDstr);
        intent.putExtra("cardNumber", cardNumber);
        intent.putExtra("expiryDate", expiryDate);
        intent.putExtra("cardDetails", cardDetails);
        intent.putExtra("price1", price1);
        intent.putExtra("price2", price2);
        intent.putExtra("price3", price3);
        intent.putExtra("price4", price4);
        startActivity(intent);

    }

    // Runs call on API to getCardDetails/'CustomerID'
    private void IDFound(Boolean found) {
        if (found) {
            Request request = new Request.Builder()
                    .url("https://e-scooter-api.herokuapp.com/getCardDetails/" + CustomerID)
                    .build();

            //Have to run httpClient call on different thread
            new Thread(() -> {
                try (Response response = httpClient.newCall(request).execute()) {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);
                    String str = response.body().string();

                    JSONObject jObject = new JSONObject(str);
                    cardNumber = jObject.getString("Card Number");
                    expiryDate = jObject.getString("Expiry Date");
                    nextActivity();

                } catch (IOException | JSONException e) {
                    cardDetails = false;
                    nextActivity();
                }
            }).start();
        } else {
            //Need to run on UI thread to avoid errors
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Invalid scooterID.", Toast.LENGTH_LONG).show());
        }
    }

    // Runs call on API to getScooterDetails/'ScooterID'
    private void enterCode(String ID) throws Exception {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getScooterDetails/" + ID)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = response.body().string();
                JSONArray jObject = new JSONArray(str);
                int available = jObject.getJSONObject(0).getInt("Available");
                if (available == 1) {
                    IDFound(true);
                } else {
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Scooter Unavailable", Toast.LENGTH_LONG).show());
                }

            } catch (IOException | JSONException e) {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Scooter Unavailable", Toast.LENGTH_LONG).show());
                IDFound(false);
            }
        }).start();
    }

    // Runs call on API to getScooterPricing/all
    public void getPrice() {
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/getScooterPricing/all ")
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                int costId = 0;
                int duration = 0;
                int price = 0;
                String str = response.body().string();
                JSONArray jObject = new JSONArray(str);
                for (int i = 0; i < jObject.length(); i++) {
                    costId = jObject.getJSONObject(i).getInt("CostID");
                    duration = jObject.getJSONObject(i).getInt("Duration(mins)");
                    price = jObject.getJSONObject(i).getInt("Price");
                    if (costId == 1) {
                        price1 = Integer.toString(price);
                    }
                    if (costId == 2) {
                        price2 = Integer.toString(price);
                    }
                    if (costId == 3) {
                        price3 = Integer.toString(price);
                    }
                    if (costId == 4) {
                        price4 = Integer.toString(price);
                    }
                }
                costFound = true;
            } catch (IOException | JSONException e) {
                costFound = false;
            }
        }).start();

    }
}