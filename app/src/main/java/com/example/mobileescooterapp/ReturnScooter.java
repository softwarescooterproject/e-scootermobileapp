package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ReturnScooter extends AppCompatActivity {

    Spinner location_menu;
    List<String> locations;
    int locationID = 0;
    String scootID;
    String customerID;
    String firstName;
    Button confirm_btn;
    Boolean scooterReturned = false;
    String scootIDstr;
    int availability;

    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_scooter);

        confirm_btn = (Button) findViewById(R.id.confirm_return_btn);
        confirm_btn.setOnClickListener(v -> {   //Set on click listener of button
            confirmReturn();
        });

        Intent i = getIntent();
        customerID = i.getStringExtra("CustomerID");
        firstName = i.getStringExtra("First Name");
        scootID = i.getStringExtra("ScooterID");

        locations = new ArrayList<String>();
        fillDropDown();
        location_menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                String item = parent.getItemAtPosition(i).toString();
                if (item == "Trinity Centre") {
                    locationID = 1;
                } else if (item == "Train Station") {
                    locationID = 2;
                } else if (item == "Merrion Centre") {
                    locationID = 3;
                } else if (item == "Leeds Royal Infirmary") {
                    locationID = 4;
                } else if (item == "The Edge") {
                    locationID = 5;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void fillDropDown() {

        locations.add("Trinity Centre");
        locations.add("Train Station");
        locations.add("Merrion Centre");
        locations.add("Leeds Royal Infirmary");
        locations.add("The Edge");

        location_menu = (Spinner) findViewById(R.id.locationSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, locations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        location_menu.setAdapter(adapter);
    }

    // Shows toast if unsuccessful, moves to MainMenu activity if it was.
    protected void scooterReturned(boolean success) {
        if (success) {
            Intent intent = new Intent(ReturnScooter.this, MainMenu.class); //Intent(thisActivity.this, destinationActivity.class)
            intent.putExtra("CustomerID", customerID);
            intent.putExtra("First Name", firstName);
            startActivity(intent);  //Go to the new activity
        } else {
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Return failed", Toast.LENGTH_LONG).show());
        }
    }

    // Runs call on API to returnScooter
    public void confirmReturn() {

        availability = 1;

        String json = "{\"Availability\": \"" + availability +
                "\", \"CurrentLocation\": \"" + locationID +
                "\", \"ScooterID\": \"" + scootID + "\" }";
        RequestBody requestBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/returnScooter")
                .addHeader("Content-Type", "application/json")
                .put(requestBody)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                scooterReturned(true);
            } catch (IOException e) {
                scooterReturned(false);
            }
        }).start();
    }

}