package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import okhttp3.*;

import java.io.IOException;
import java.io.Serializable;

public class LogIn extends AppCompatActivity {

    Button logInButton;
    EditText email;
    EditText password;
    String CustomerID;
    String FirstName;


    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    boolean accountFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        logInButton = (Button) findViewById(R.id.confirm_log_in_btn);   //Initialise the button
        email = (EditText) findViewById((R.id.editTextEmailLogIn));
        password = (EditText) findViewById((R.id.editTextPasswordLogIn));

        logInButton.setOnClickListener(v -> {   //Set on click listener of button
            String emailStr = email.getText().toString();
            String passwordStr = password.getText().toString();

            if (!emailStr.isEmpty() && !passwordStr.isEmpty()) {
                try {
                    login(emailStr, passwordStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Both email and password must not be empty", Toast.LENGTH_LONG).show());
            }
        });
    }

    // Shows toast if unsuccessful, moves to MainMenu activity if it was.
    protected void accountFound(Boolean found) {
        if (found) {
            Intent intent = new Intent(LogIn.this, MainMenu.class);
            intent.putExtra("CustomerID", CustomerID);
            intent.putExtra("First Name", FirstName);
            startActivity(intent);
        } else {
            //Need to run on UI thread to avoid errors
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Invalid email and password combination.", Toast.LENGTH_LONG).show());
        }
    }

    // Runs call on API to login
    protected void login(String email, String password) throws Exception {

        String json = "{\"Email\": \"" + email + "\", \"Password\" : \"" + password + "\"}";
        RequestBody requestBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/login ")
                .addHeader("Content-Type", "application/json")
                .post(requestBody)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                String str = response.body().string();
                JSONArray jObject = new JSONArray(str);
                CustomerID = jObject.getJSONObject(0).getString("CustomerID");
                int discount = jObject.getJSONObject(0).getInt("Discount");
                String emailApi = jObject.getJSONObject(0).getString("Email");
                FirstName = jObject.getJSONObject(0).getString("First Name");

                accountFound(true);
            } catch (IOException | JSONException e) {
                accountFound(false);
            }
        }).start();
    }
}