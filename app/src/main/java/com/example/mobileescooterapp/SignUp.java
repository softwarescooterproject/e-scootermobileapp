package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SignUp extends AppCompatActivity {

    Button signUpBtn;
    EditText firstName;
    EditText lastName;
    EditText email;
    EditText password;
    CheckBox discount;
    int discountVal = 0;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private final OkHttpClient httpClient = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        signUpBtn = (Button) findViewById(R.id.sign_up_btn);

        firstName = (EditText) findViewById((R.id.editTextFirstName));
        lastName = (EditText) findViewById((R.id.editTextLastName));
        email = (EditText) findViewById((R.id.editTextEmailSignUp));
        password = (EditText) findViewById((R.id.editTextPasswordSignUp));
        discount = (CheckBox) findViewById(R.id.discountCheckBox);

        discount.setOnClickListener(v -> {
            if (discount.isChecked()) {
                discountVal = 1;
            } else {
                discountVal = 0;
            }

        });


        signUpBtn.setOnClickListener(v -> {   //Set on click listener of button
            String firstNameStr = firstName.getText().toString();
            String lastNameStr = lastName.getText().toString();
            String emailStr = email.getText().toString();
            String passwordStr = password.getText().toString();

            if (!emailStr.isEmpty() && !passwordStr.isEmpty() && !firstNameStr.isEmpty() && !lastNameStr.isEmpty()) {
                try {
                    signUp(firstNameStr, lastNameStr, emailStr, passwordStr, discountVal);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Fields cannot be empty", Toast.LENGTH_LONG).show());
            }

        });

    }

    // Shows toast if unsuccessful, moves to AccountCreated activity if it was.
    protected void accountCreated(Boolean success) {
        if (success) {
            Intent intent = new Intent(SignUp.this, AccountCreated.class);
            startActivity(intent);
        } else {
            //Need to run on UI thread to avoid errors
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Account not created", Toast.LENGTH_LONG).show());
        }
    }

    // Runs call on API to addNewAccount
    private void signUp(String firstName, String secondName, String email, String password, int discount) throws Exception {

        String json = "{\"FirstName\": \"" + firstName +
                "\", \"SecondName\" : \"" + secondName +
                "\", \"Email\": \"" + email +
                "\", \"Discount\": \"" + discount +
                "\", \"Password\" : \"" + password + "\"}";

        RequestBody requestBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/addNewAccount ")
                .addHeader("Content-Type", "application/json")
                .post(requestBody)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                accountCreated(true);
            } catch (IOException e) {
                accountCreated(false);
            }
        }).start();
    }
}