package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddCard extends AppCompatActivity {

    Button continueButton;
    EditText cardNumEditText;
    EditText expiryDateEditText;
    String CustomerId;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private final OkHttpClient httpClient = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        cardNumEditText = (EditText) findViewById(R.id.cardNumberAddCard);
        expiryDateEditText = (EditText) findViewById(R.id.expiryDateAddCard);
        continueButton = (Button) findViewById(R.id.continue_card_mainMenu_btn);   //Initialise the button

        CustomerId = getIntent().getStringExtra("CustomerId");

        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                    String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 4; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 6) {
                        clean = clean + yyyymm.substring(clean.length());
                    } else {
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        int year = Integer.parseInt(clean.substring(0, 4));
                        int mon = Integer.parseInt(clean.substring(4, 6));

                        mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                        cal.set(Calendar.MONTH, mon - 1);
                        year = (year < 2022) ? 2022 : (year > 2100) ? 2100 : year;
                        cal.set(Calendar.YEAR, year);
                        // ^ first set year for the line below to work correctly
                        //with leap years - otherwise, date e.g. 29/02/2012
                        //would be automatically corrected to 28/02/2012

                        clean = String.format("%02d%02d", year, mon);
                    }

                    clean = String.format("%s-%s", clean.substring(0, 4),
                            clean.substring(4, 6));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    expiryDateEditText.setText(current);
                    expiryDateEditText.setSelection(sel < current.length() ? sel : current.length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

            private String current = "";
            private String yyyymm = "YYYYMM";
            private Calendar cal = Calendar.getInstance();
        };

        expiryDateEditText.addTextChangedListener(tw);

        continueButton.setOnClickListener(v -> {   //Set on click listener of button
            String cardNumStr = cardNumEditText.getText().toString();
            String expiryDateStr = expiryDateEditText.getText().toString();
            if (cardNumStr.length() < 13) {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Card number is not long enough", Toast.LENGTH_LONG).show());
            } else if (expiryDateStr.matches("\\d\\d\\d\\d-\\d\\d")) {
                if (!cardNumStr.isEmpty() && !expiryDateStr.isEmpty()) {
                    try {
                        addCard(CustomerId, expiryDateStr, cardNumStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "All fields must be filled correctly", Toast.LENGTH_LONG).show());
            }

        });
    }

    //
    protected void addCard(String customerId, String expiryDate, String cardNumber) {
        String json = "{\"CustomerID\": \"" + customerId +
                "\", \"ExpiryDate\": \"" + expiryDate + "-01" +
                "\", \"CardNumber\": \"" + cardNumber + "\"}";

        RequestBody requestBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/addNewCardDetails ")
                .addHeader("Content-Type", "application/json")
                .post(requestBody)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                cardAdded(true);
            } catch (IOException e) {
                cardAdded(false);
            }
        }).start();
    }

    protected void cardAdded(Boolean success) {
        if (success) {
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Card was added successfully", Toast.LENGTH_LONG).show());
            Intent intent = new Intent(AddCard.this, MainMenu.class); //Intent(thisActivity.this, destinationActivity.class)
            intent.putExtra("CustomerID", getIntent().getStringExtra("CustomerId"));
            intent.putExtra("First Name", getIntent().getStringExtra("First Name"));
            startActivity(intent);  //Go to the new activity
        } else {
            //Need to run on UI thread to avoid errors
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Card was not added successfully", Toast.LENGTH_LONG).show());
        }
    }

}