package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class BookingConf extends AppCompatActivity {

    Button continue_btn;
    String CustomerID;
    String FirstName;
    String costID;
    int costIDInt;
    String bookedFor;
    TextView bookedForTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_conf);

        setBookedFor();


        continue_btn = (Button) findViewById(R.id.continue_bookingconf_btn);   //Initialise the button
        continue_btn.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(BookingConf.this, MainMenu.class); //Intent(thisActivity.this, destinationActivity.class)
            intent.putExtra("CustomerID", getIntent().getStringExtra("CustomerID"));
            intent.putExtra("First Name", getIntent().getStringExtra("First Name"));
            startActivity(intent);  //Go to the new activity
        });

    }

    // Populates intent for moving back to the MainMenu activity
    protected void setBookedFor() {

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");
        costID = i.getStringExtra("costID");

        if (costID == null) { //this is for testing
            costID = "1";
        }
        costIDInt = Integer.valueOf(costID);
        if (costIDInt == 1) {
            costID = "1 hour";
        } else if (costIDInt == 2) {
            costID = "4 hours";
        } else if (costIDInt == 3) {
            costID = "1 day";
        } else {
            costID = "1 week";
        }

        bookedForTxt = (TextView) findViewById(R.id.id_booked_for);
        bookedForTxt.setText("Scooter booked for: " + costID + "!");

    }
}