package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendFeedback extends AppCompatActivity {

    Button continue_btn;
    EditText feedback;

    String CustomerID;
    String FirstName;

    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    boolean accountFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_feedback);

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");

        continue_btn = (Button) findViewById(R.id.continue_sendfeedback_btn);//Initialise the button
        feedback = (EditText) findViewById(R.id.enterFeedbackText);

        continue_btn.setOnClickListener(v -> {//Set on click listener of button
            String feedbackStr = feedback.getText().toString();

            if (!feedbackStr.isEmpty()) {
                try {
                    sendFeedback(feedbackStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // Shows toast if unsuccessful, moves to MainMenu activity if it was.
    protected void feedbackSent(Boolean success) {
        if (success) {
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Feedback Sent Successfully", Toast.LENGTH_LONG).show());
            Intent intent = new Intent(SendFeedback.this, MainMenu.class);
            intent.putExtra("CustomerID", getIntent().getStringExtra("CustomerID"));
            intent.putExtra("First Name", getIntent().getStringExtra("First Name"));
            startActivity(intent);
        } else {
            //Need to run on UI thread to avoid errors
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Unable to Send Feedback", Toast.LENGTH_LONG).show());
        }
    }

    // Runs call on API to addNewFeedback
    protected void sendFeedback(String feedbackStr) throws Exception {

        String json = "{\"UserId\": \"" + CustomerID +
                "\", \"FeedbackContents\": \"" + feedbackStr + "\"}";
        RequestBody requestBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder()
                .url("https://e-scooter-api.herokuapp.com/addNewFeedback ")
                .addHeader("Content-Type", "application/json")
                .post(requestBody)
                .build();

        //Have to run httpClient call on different thread
        new Thread(() -> {
            try (Response response = httpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String str = response.body().string();
                feedbackSent(true);
            } catch (IOException e) {
                feedbackSent(false);
            }
        }).start();
    }
}
