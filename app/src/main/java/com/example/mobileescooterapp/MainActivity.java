package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    Button logInButton;
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); //Ignores a phones dark mode settings

        logInButton = (Button) findViewById(R.id.go_to_log_in_btn);   //Initialise the button
        logInButton.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(MainActivity.this, LogIn.class); //Intent(thisActivity.this, destinationActivity.class)
            startActivity(intent);  //Go to the new activity
        });

        signUpButton = (Button) findViewById(R.id.go_to_sign_up_btn);   //Initialise the button
        signUpButton.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(MainActivity.this, SignUp.class); //Intent(thisActivity.this, destinationActivity.class)
            startActivity(intent);  //Go to the new activity
        });
    }


    @Override
    public void onBackPressed() {
        Toast.makeText(MainActivity.this, "Invalid action", Toast.LENGTH_LONG).show();
        return;
    }
}