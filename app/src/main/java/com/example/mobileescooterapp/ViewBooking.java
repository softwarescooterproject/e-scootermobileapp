package com.example.mobileescooterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.*;

public class ViewBooking extends AppCompatActivity {

    Button mainMenuBtn;
    TextView idTxt;
    TextView bookingLengthTxt;
    TextView timeRemainingTxt;
    TextView overdueTxt;
    Button returnScooterBtn;
    Button extendDurationBtn;

    String CustomerID;
    String FirstName;
    String dateHired;
    String length;
    String costID;
    String scooterID;
    int sessionID;
    String timeHired;
    String bookingTime;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final OkHttpClient httpClient = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_booking);

        Intent i = getIntent();
        CustomerID = i.getStringExtra("CustomerID");
        FirstName = i.getStringExtra("First Name");
        costID = i.getStringExtra("costID");
        scooterID = i.getStringExtra("scooterID");
        timeHired = i.getStringExtra("timeHired");
        dateHired = i.getStringExtra("dateHired");

        Bundle extras = i.getExtras();
        if (extras == null) { //for testing purposes - will need to be updated if booking expires
            CustomerID = "104";
            scooterID = "106";
            costID = "3";
            timeHired = "16:20:03";
            dateHired = "2022-04-22";
        }

        bookingTime = dateHired + " " + timeHired;

        Date now = new Date();
        Date returnTime = findBookingLength().getTime();
        SimpleDateFormat outputFormat = new SimpleDateFormat("MM/dd HH:mm");

        bookingLengthTxt = (TextView) findViewById(R.id.bookingLength);
        bookingLengthTxt.setText("Booking length: " + length);


        idTxt = (TextView) findViewById(R.id.scooterIDText);
        idTxt.setText("Scooter ID: " + scooterID);

        returnScooterBtn = (Button) findViewById(R.id.returnScooterBtn);
        returnScooterBtn.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(ViewBooking.this, ReturnScooter.class);
            intent.putExtra("CustomerID", CustomerID);
            intent.putExtra("First Name", FirstName);
            intent.putExtra("ScooterID", scooterID);
            startActivity(intent);
        });

        extendDurationBtn = (Button) findViewById(R.id.extend_duration_btn);
        extendDurationBtn.setOnClickListener(v -> {   //Set on click listener of button
            if (costID.equals("4")) {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), "The booking cannot be extended further", Toast.LENGTH_LONG).show());
            } else {
                Intent intent = new Intent(ViewBooking.this, ExtendBooking.class);
                intent.putExtra("CustomerID", CustomerID);
                intent.putExtra("First Name", FirstName);
                intent.putExtra("ScooterID", scooterID);
                intent.putExtra("CostID", costID);
                startActivity(intent);
            }
        });

        timeRemainingTxt = (TextView) findViewById(R.id.timeRemainingText);
        timeRemainingTxt.setText("Return by " + outputFormat.format(returnTime));

        if (now.getTime() > returnTime.getTime()) {
            overdueTxt = (TextView) findViewById(R.id.overdueText);
            overdueTxt.setVisibility(View.VISIBLE);
        }

        mainMenuBtn = (Button) findViewById(R.id.go_to_main_menu_btn);
        mainMenuBtn.setOnClickListener(v -> {   //Set on click listener of button
            Intent intent = new Intent(ViewBooking.this, MainMenu.class);
            intent.putExtra("CustomerID", CustomerID);
            intent.putExtra("First Name", FirstName);
            startActivity(intent);
        });


    }

//    Extend session, check cost id is greater prev and less than 1 week
    private Calendar findBookingLength() {
        Date timeBooked = null;
        Calendar c = Calendar.getInstance();
        try {
            timeBooked = format.parse(bookingTime);
            c.setTime(timeBooked);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (costID.equals("1")) {
            length = "1 hour";
            c.add(Calendar.HOUR, 1);
        } else if (costID.equals("2")) {
            length = "4 hours";
            c.add(Calendar.HOUR, 4);
        } else if ((costID.equals("3"))) {
            length = "1 day";
            c.add(Calendar.DATE, 1);
        } else if ((costID.equals("4"))) {
            length = "1 week";
            c.add(Calendar.DATE, 7);
        } else {
            length = "invalid booking";
        }
        return c;
    }
}