# E-ScooterMobileApp

This code base is intended to be run through Android Studio. Android Studio is the official Integrated Development Environment (IDE) for Android app development. It can be donwloaded at (including all relevant Java SDK's): https://developer.android.com/studio.

This repo uses Gradle files to automatically build the application, install it's dependancies, compile the code and deploy it to an Android device, physical or virtually emulated. 

The only thing to add is the following API key into the local.properties file in order to enable the map functionality:
    MAPS_API_KEY=AIzaSyBv0Lc3aMzOpisNfkYmebrD4UDiDkSVTQ
